<?php

/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * Keyboard Defaults Behavior.
 */
class OpenlayersBehaviorOKC extends openlayers_behavior {

  /**
   * Provide initial values for options.
   */
  public function options_init() {
    return array(
      'keyboarddefaults' => '',
    );
  }

  /**
   * Render.
   */
  public function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'ol_lat_long') . '/js/openlayers_behavior_OKC.js');
    return $this->options;
  }

}
